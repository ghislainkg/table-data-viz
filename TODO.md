* Add visual informations on bars : So that just the image gives all informations
* add column packing : For same unit columns and etc ...
* add custom columns by aggrageting values of a column pack : mean, max, min, standard deviation
* add support for value types :
  * numeric
  * text
  * data
* add the possibility to display multiple charts
* save/load charts from the same file as data
* see for other useful visualisation other than bar charts
