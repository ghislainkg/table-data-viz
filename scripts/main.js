
const tableView = new TableView('table-container')
tableView.table.clearColumns()
tableView.table.clearEntries()
tableView.table.clearPacks()

tableView.reloadTablePacks()
tableView.reloadTableHeader()
tableView.reloadTableEntries()

function openJsonText(text) {
  const json = JSON.parse(text)
  tableView.table.clearPacks()
  tableView.table.clearColumns()
  tableView.table.clearEntries()
  tableView.table.fromJson(json)
  tableView.reloadTablePacks()
  tableView.reloadTableHeader()
  tableView.reloadTableEntries()
}

var tableViz = null
var dialogChooseLabelColumn = new DialogChooseColumn("Choose label column", false, true)
tableView.onVizPack = (pack=new TableColumnPack()) => {
  dialogChooseLabelColumn.open()
  dialogChooseLabelColumn.update(tableView.table)
  dialogChooseLabelColumn.onConfirm = (cols=[new TableColumn()]) => {
    dialogChooseLabelColumn.close()
    if(tableViz) {
      tableViz.destroy()
    }
    tableViz = new TableViz(
      'table-viz-canvas', 'table-viz-container',
      tableView.table,
      cols[0], pack.columns
    )
  }
}

const tableViewPacks = new TableView('table-container', false, false)
tableViewPacks.table.clearColumns()
tableViewPacks.table.clearEntries()
tableViewPacks.table.clearPacks()
tableViewPacks.reloadTablePacks()
tableViewPacks.reloadTableHeader()
tableViewPacks.reloadTableEntries()
const buttonUpdatePackTable = document.getElementById('button-update-packs-table')
buttonUpdatePackTable.addEventListener('click', () => {
  tableViewPacks.table.clearPacks()
  tableViewPacks.table.clearColumns()
  tableViewPacks.table.clearEntries()

  const packsTable = tableView.table.generateTable()
  tableViewPacks.table = packsTable

  tableViewPacks.reloadTablePacks()
  tableViewPacks.reloadTableHeader()
  tableViewPacks.reloadTableEntries()
})

const buttonSave = document.getElementById('button-save-file')
buttonSave.addEventListener('click', () => {
  const json = tableView.table.toJson()
  text = JSON.stringify(json)
  var a = document.createElement('a');
  var file = new Blob([text], {type: 'text/json'});
  a.href = URL.createObjectURL(file);
  a.download = 'table.json';
  a.click()
})

const buttonOpen = document.getElementById('button-open-file')
buttonOpen.addEventListener('click', () => {
  let input = document.createElement('input');
  input.type = 'file';
  input.onchange = () => {
    let files = Array.from(input.files);
    files[0].text().then((text) => {
      openJsonText(text)
    })
  };
  input.click();
})
