// ----------------------------------------------------
// View
// ----------------------------------------------------

function TableHeaderView(tableView = new TableView(""), editEnabled=true) {
	var tr = document.createElement('tr')
	var ths = [document.createElement('th')]
	var buttonAddColumn = document.createElement('button')
	var tableView = tableView

	tableView.tableElem.appendChild(tr)

	buttonAddColumn.innerText = "Add column"
	buttonAddColumn.addEventListener('click', () => {
		tableView._onClickHeaderAddColumn()
	})

	var _clearThs = function () {
		while(tr.firstChild) {
			tr.removeChild(tr.lastChild)
		}
		ths.splice(0,ths.length)
	}
	var _create_th = function (col=new TableColumn()) {
		const th = document.createElement('th')
		th.innerHTML = `${col.name} (${col.unit})`
		if(editEnabled) {
			th.addEventListener('click', () => {
				tableView._onClickHeaderColumn(col)
			})
		}
		return th
	}

	this._loadColumns = function (columns = [new TableColumn()]) {
		_clearThs()
		columns.forEach(col => {
			const th = _create_th(col)
			ths.push(th)
			tr.appendChild(th)
		})
		if(editEnabled) tr.appendChild(buttonAddColumn)
	}
	this._removeEntry = function () {
		tableView.tableElem.removeChild(tr)
	}
}


function TablePackView (tableView = new TableView(""), editEnabled=true) {
	var tr = document.createElement('tr')
	var tds = [document.createElement('td')]
	var tableView = tableView

	tableView.tablePacksElem.appendChild(tr)

	var _clearTds = function () {
		while(tr.firstChild) {
			tr.removeChild(tr.lastChild)
		}
		tds.splice(0,tds.length)
	}
	var _create_pack_td = function (pack=new TableColumnPack()) {
		const td = document.createElement('td')
		td.style.fontWeight = 'bolder'
		td.innerHTML = `${pack.virtualColumn.name} [${pack.operation}] (${pack.virtualColumn.unit})`
		if(editEnabled) {
			td.addEventListener('click', () => {
				tableView._onClickPack(pack)
			})
		}
		return td
	}
	var _create_column_td = function (col=new TableColumn()) {
		const td = document.createElement('td')
		td.innerHTML = `${col.name} (${col.unit})`
		return td
	}

	this._loadPack = function (pack=new TableColumnPack()) {
		_clearTds()
		const packTd = _create_pack_td(pack)
		tr.appendChild(packTd)
		pack.columns.forEach(col => {
			const td = _create_column_td(col)
			tds.push(td)
			tr.appendChild(td)
		})

		const vizButton = document.createElement('button')
		vizButton.innerText = "Visualize"
		vizButton.addEventListener('click', () => {
			tableView.onVizPack(pack)
		})
		tr.appendChild(vizButton)
	}
	this._removePack = function () {
		tableView.tablePacksElem.removeChild(tr)
	}
}

function TableEntryView(tableView = new TableView(""), editEnabled=true) {
	var currentEntry = new TableEntry()
	var tr = document.createElement('tr')
	var tds = [document.createElement('td')]
	var tableView = tableView

	tableView.tableElem.appendChild(tr)

	var buttonRemove = document.createElement('button')
	buttonRemove.innerText = "X"
	buttonRemove.addEventListener('click', () => {
		tableView._onClickRemoveEntry(currentEntry)
	})

	var _clearTds = function () {
		while(tr.firstChild) {
			tr.removeChild(tr.lastChild)
		}
		tds.splice(0,tds.length)
	}

	var _create_td = function (entry=new TableEntry, col=new TableColumn()) {
		const td = document.createElement('td')
		td.innerHTML = entry.columnValues[col.name]
		if(editEnabled) {
			td.addEventListener('click', () => {
				tableView._onClickEntryValue(entry, col)
			})
		}
		return td
	}

	this._loadEntry = function (entry = new TableEntry()) {
		_clearTds()
		entry._table._columns.forEach(col => {
			const td = _create_td(entry, col)
			tds.push(td)
			tr.appendChild(td)
		})
		if(editEnabled) {
			tr.appendChild(buttonRemove)
		}
		currentEntry = entry
	}
	this._removeEntry = function () {
		tableView.tableElem.removeChild(tr)
	}
}

function TableView(containerId, editEnabled=true, withPacks=true) {
	this.table = new Table()
	this.tablePacksElem = document.createElement('table')
	this.tableElem = document.createElement('table')
	var buttonAddPack = document.createElement('button')
	var buttonAddEntryEdit = document.createElement('button')
	var buttonAddEntry = document.createElement('button')

	if(editEnabled) {
		this.tablePacksElem.appendChild(buttonAddPack)
		this.tableElem.appendChild(buttonAddEntryEdit)
		this.tableElem.appendChild(buttonAddEntry)
	}

	var containerId = containerId
	var tablePackViews = [new TablePackView(this, editEnabled)]
	var tableHeaderView = new TableHeaderView(this, editEnabled)
	var tableEntryViews = [new TableEntryView(this, editEnabled)]

	if(withPacks) {
		document.getElementById(containerId).appendChild(this.tablePacksElem)
	}
	document.getElementById(containerId).appendChild(this.tableElem)

	this.onVizPack = (pack = new TableColumnPack()) => {}

	buttonAddPack.innerText = "Add columns pack"
	buttonAddPack.addEventListener('click', () => {
		const pack = this.table.addPack()
		const dialog = new DialogEditPack(pack)
		dialog.open()
		dialog.onConfirm = () => {
			dialog.close()
			this.reloadTablePacks()
		}
	})
	buttonAddEntryEdit.innerText = "Add entry edit"
	buttonAddEntryEdit.addEventListener('click', () => {
		const entry = this.table.addRow()
		const dialog = new DialogEditEntry(entry, "")
		dialog.onValidateEditEntryValue = () => {
			this.reloadTableHeader()
			this.reloadTableEntries()
		}
	})
	buttonAddEntry.innerText = "Add entry"
	buttonAddEntry.addEventListener('click', () => {
		this.table.addRow()
		this.reloadTableHeader()
		this.reloadTableEntries()
	})
	this._onClickHeaderAddColumn = (col=new TableColumn()) => {
		const dialog = new DialogAddColumn()
		dialog.onValidateNewCol = (name, unit) => {
			this.table.addCol(name, TableColumnType.NUMERIC, unit)
			this.reloadTableHeader()
			this.reloadTableEntries()
		}
	}
	this._onClickHeaderColumn = (col=new TableColumn()) => {
		const dialog = new DialogEditColumn(col)
		dialog.onMoveLeft = () => {
			this.table.moveColumn(col, -1)
			this.reloadTableHeader()
			this.reloadTableEntries()
		}
		dialog.onMoveRight = () => {
			this.table.moveColumn(col, 1)
			this.reloadTableHeader()
			this.reloadTableEntries()
		}
		dialog.onDelete = () => {
			this.table.removeColumn(col)
			this.reloadTableHeader()
			this.reloadTableEntries()
		}
		dialog.onEdit = (name="", unit="") => {
			this.table.updateColumn(col, name, unit)
			this.reloadTableHeader()
			this.reloadTableEntries()
		}
	}
	this._onClickPack = (pack=new TableColumnPack()) => {
		const dialog = new DialogEditPack(pack)
		dialog.open()
		dialog.onConfirm = () => {
			dialog.close()
			this.reloadTablePacks()
		}
	}
	this._onClickEntryValue = (entry=new TableEntry(), col=new TableColumn()) => {
		const dialog = new DialogEditEntry(entry, col.name)
		dialog.onValidateEditEntryValue = () => {
			this.reloadTableHeader()
			this.reloadTableEntries()
		}
	}
	this._onClickRemoveEntry = (entry=new TableEntry) => {
		this.table.removeEntry(entry)
		this.reloadTableHeader()
		this.reloadTableEntries()
	}

	this.reloadTableHeader = function () {
		tableHeaderView._loadColumns(this.table._columns)
	}
	this.reloadTableEntries = function () {
		_clearEntries()
		this.table._entries.forEach(entry => {
			const entryView = new TableEntryView(this, editEnabled)
			entryView._loadEntry(entry)
			tableEntryViews.push(entryView)
		})
	}
	this.reloadTablePacks = function () {
		_clearPacks()
		if(withPacks) {
			this.table._packs.forEach(pack => {
				const packView = new TablePackView(this, editEnabled)
				packView._loadPack(pack)
				tablePackViews.push(packView)
			})
		}
	}

	var _clearEntries = function () {
		tableEntryViews.forEach((view) => {
			view._removeEntry()
		})
		tableEntryViews.splice(0,tableEntryViews.length)
	}
	var _clearPacks = function () {
		tablePackViews.forEach((view) => {
			view._removePack()
		})
		tablePackViews.splice(0,tablePackViews.length)
	}
}


// ----------------------------------------------------
// Dialogs
// ----------------------------------------------------

function DialogAddColumn () {
	var div = document.createElement('div')
	var content = document.createElement('div')
	var inputName = document.createElement('input')
	var inputUnit = document.createElement('input')
	content.appendChild(inputName)
	content.appendChild(inputUnit)
	div.appendChild(content)
	div.classList.add('dialog-div')

	var body = document.getElementsByTagName('body').item(0)
	body.appendChild(div)

	inputName.placeholder = "Column name"
	inputUnit.placeholder = "Column unit"
	inputName.focus()

	div.addEventListener('click', (e) => {
		if(e.target.classList.contains('dialog-div')) {
			close()
		}
	})
	inputName.addEventListener('keyup', (e) => {
		if(e.key=='Enter') {
			this.onValidateNewCol(inputName.value, inputUnit.value)
			inputName.value = ""
			inputUnit.value = ""
		}
	})
	inputUnit.addEventListener('keyup', (e) => {
		if(e.key=='Enter') {
			this.onValidateNewCol(inputName.value, inputUnit.value)
			inputName.value = ""
			inputUnit.value = ""
		}
	})

	this.onValidateNewCol = (name, unit) => {}

	var close = function () {
		body.removeChild(div)
	}
}
function DialogEditColumn (col=new TableColumn()) {
	var div = document.createElement('div')
	var content = document.createElement('div')
	var buttonLeft = document.createElement('button')
	var buttonDelete = document.createElement('button')
	var buttonRight = document.createElement('button')
	var inputName = document.createElement('input')
	var inputUnit = document.createElement('input')
	buttonLeft.innerText = "<- Move Left"
	buttonDelete.innerText = "Delete"
	buttonRight.innerText = "Move Right ->"
	content.appendChild(buttonLeft)
	content.appendChild(buttonDelete)
	content.appendChild(buttonRight)
	content.appendChild(inputName)
	content.appendChild(inputUnit)
	div.appendChild(content)
	div.classList.add('dialog-div')
	var body = document.getElementsByTagName('body').item(0)
	body.appendChild(div)

	inputName.value = col.name
	inputUnit.value = col.unit

	inputName.placeholder = "Column name"
	inputUnit.placeholder = "Column unit"

	div.addEventListener('click', (e) => {
		if(e.target.classList.contains('dialog-div')) {
			close()
		}
	})
	buttonLeft.addEventListener('click', (e) => {
		this.onMoveLeft()
	})
	buttonRight.addEventListener('click', (e) => {
		this.onMoveRight()
	})
	buttonDelete.addEventListener('click', (e) => {
		if(window.confirm("Are you sure you want to delete this column ?")==true) {
			this.onDelete()
			close()
		}
	})
	inputName.addEventListener('keyup', (e) => {
		if(e.key=='Enter') {
			this.onEdit(inputName.value, inputUnit.value)
		}
	})
	inputUnit.addEventListener('keyup', (e) => {
		if(e.key=='Enter') {
			this.onEdit(inputName.value, inputUnit.value)
		}
	})

	this.onMoveLeft = () => {}
	this.onMoveRight = () => {}
	this.onDelete = () => {}
	this.onEdit = (name="", unit="") => {}
	var close = function () {
		body.removeChild(div)
	}
}

function DialogEditEntry (entry=new TableEntry(), focusColName="") {
	var div = document.createElement('div')
	var content = document.createElement('div')
	content.classList.add('dialog-edit-entry-content')

	div.appendChild(content)
	div.classList.add('dialog-div')
	var body = document.getElementsByTagName('body').item(0)
	body.appendChild(div)

	var editedValues = {}

	entry._table._columns.forEach((col, i) => {
		editedValues[col.name] = entry.columnValues[col.name]

		const inputName = document.createElement('input')
		inputName.value = entry.columnValues[col.name]
		inputName.addEventListener('keyup', (e) => {
			editedValues[col.name] = inputName.value
			if(e.key=='Enter') {
				entry._table._columns.forEach((c) => {
					entry.columnValues[c.name] = editedValues[c.name]
				})
				this.onValidateEditEntryValue()

				close()
			}
		})
		inputName.addEventListener('focusin', () => {
			inputName.select();
		})

		const colDiv = document.createElement('div')
		const label = document.createElement('label')
		label.innerText = col.name
		colDiv.appendChild(label)
		colDiv.appendChild(inputName)
		content.appendChild(colDiv)

		if(focusColName.length==0) {
			if(i==0) inputName.focus()
		}
		else {
			if(focusColName===col.name) {
				inputName.focus()
			}
		}
	})

	div.addEventListener('click', (e) => {
		if(e.target.classList.contains('dialog-div')) {
			close()
		}
	})

	this.onValidateEditEntryValue = () => {}

	var close = function () {
		body.removeChild(div)
	}
}


function ChooseColumnView (title="", multiple=false, atLeastOne=true) {
	this.content = document.createElement('div')
	var titleLabel = document.createElement('label')
	this.content.classList.add('choose-column-view-content')
	titleLabel.innerText = title

	// Selected columns list
	this.selection = [new TableColumn()]
	this.selection.splice(0,1)
	// Buttons for all selectable columns
	var buttons = [{elem:document.createElement('button'),col:new TableColumn()}]
	buttons.splice(0,1)

	// Change the selected/unselected view of all columns buttons
	var updateButtons = () => {
		buttons.forEach(button => {
			if(this.selection.indexOf(button.col)>=0) button.elem.classList.add('selected')
			else button.elem.classList.remove('selected')
		})
	}

	// Update the selected columns according to the buttons :
	// useful when the set of columns changes and we want to
	// remove old selected columns
	var syncSelection = () => {
		this.selection = this.selection.filter(
			(col) => buttons.findIndex(button => button.col==col)>=0
		)
	}

	/*Erase and recreate buttons for the columns of the table.*/
	this.update = function (table=new Table()) {
		while(this.content.firstChild) this.content.removeChild(this.content.lastChild)
		this.content.appendChild(titleLabel)
		buttons.splice(0,buttons.length)

		table._columns.forEach((col, currIndex) => {
			var button = document.createElement('button')
			button.innerText = col.name
			this.content.appendChild(button)
			buttons.push({elem:button, col:col})

			if(atLeastOne) {
				// Select the first one
				if(currIndex==0 && this.selection.length==0) {
					this.selection.push(col)
				}
			}

			button.addEventListener('click', (e) => {
				// On column button click:

				// We decide if we selected or unselecte the column according to
				// multiple and atLeastOne selection policy
				const index = this.selection.indexOf(col)
				if(index>=0) {
					if(atLeastOne) {
						if(multiple) {
							if(this.selection.length>1) {
								this.selection.splice(index,1)
							}
						}
					}
					else {
						if(multiple) {
							this.selection.splice(index,1)
						}
					}
				}
				else {
					if(multiple) {
						this.selection.push(col)
					}
					else {
						if(this.selection.length>0) {
							this.selection.splice(0,this.selection.length)
						}
						this.selection.push(col)
					}
				}

				// We update the buttons selection states
				updateButtons()
			})

			updateButtons()
		})
		syncSelection()
	}
}

function DialogChooseColumn (title="", multiple=false, atLeastOne=true) {
	var div = document.createElement('div')
	div.classList.add('dialog-div')

	var content = document.createElement('div')
	content.classList.add('dialog-choose-column-content')

	var chooseView = new ChooseColumnView (title, multiple, atLeastOne)
	content.appendChild(chooseView.content)
	
	this.update = function (table=new Table()) {
		chooseView.update(table)
	}

	var buttonConfirm = document.createElement('button')
	buttonConfirm.innerText = "Confirm"
	content.appendChild(document.createElement('br'))
	content.appendChild(buttonConfirm)
	buttonConfirm.addEventListener('click', () => {
		this.onConfirm(chooseView.selection)
	})
	this.onConfirm = (selection=[new TableColumn()]) => {}


	div.appendChild(content)
	var body = document.getElementsByTagName('body').item(0)
	div.addEventListener('click', (e) => {
		if(e.target.classList.contains('dialog-div')) {
			this.close()
		}
	})

	this.close = function () {
		body.removeChild(div)
	}
	this.open = function () {
		body.appendChild(div)
	}
}

function DialogEditPack (pack=new TableColumnPack()) {
	var div = document.createElement('div')
	div.classList.add('dialog-div')

	var content = document.createElement('div')
	content.classList.add('dialog-edit-pack-content')
	var editDiv = document.createElement('div')
	editDiv.classList.add('edit-div')

	var inputName = document.createElement('input')
	var inputUnit = document.createElement('input')
	inputName.value = pack.virtualColumn.name
	inputUnit.value = pack.virtualColumn.unit
	inputName.placeholder = "Pack virtual column name"
	inputUnit.placeholder = "Pack virtual column unit"
	editDiv.appendChild(inputName)
	editDiv.appendChild(inputUnit)

	var operationSelect = document.createElement('select')
	operationSelect.name = 'operation'
	operationSelect.multiple = false
	operationSelect.ariaPlaceholder = "Operation"
	Object.keys(TablePackOperation).forEach(op => {
		const operation = TablePackOperation[op]
		var option = document.createElement('option')
		option.value = operation
		option.innerText = op
		if(pack.operation==operation) {
			option.selected = true
		}
		operationSelect.appendChild(option)
	})
	editDiv.appendChild(operationSelect)

	content.appendChild(editDiv)

	var chooseValueCols = new ChooseColumnView ("Value columns", true, true)
	pack.columns.forEach(col => {
		chooseValueCols.selection.push(col)
	})
	chooseValueCols.update(pack._table)
	content.appendChild(chooseValueCols.content)

	var buttonConfirm = document.createElement('button')
	buttonConfirm.innerText = "Confirm"
	editDiv.appendChild(buttonConfirm)
	buttonConfirm.addEventListener('click', () => {
		pack.clearColumns()
		chooseValueCols.selection.forEach(col => {
			pack.addColumn(col)
		})
		pack.virtualColumn.name = inputName.value
		pack.virtualColumn.unit = inputUnit.value
		pack.operation = operationSelect.value
		this.onConfirm()
	})

	this.onConfirm = () => {}

	div.appendChild(content)
	var body = document.getElementsByTagName('body').item(0)
	div.addEventListener('click', (e) => {
		if(e.target.classList.contains('dialog-div')) {
			this.close()
		}
	})

	this.close = function () {
		body.removeChild(div)
	}
	this.open = function () {
		body.appendChild(div)
	}
}
