// ----------------------------------------------------
// Model
// ----------------------------------------------------

// The type of values
const TableColumnType = Object.freeze({
	NUMERIC: "numeric",
	TEXT: "text",
  DATE: "date",
})

// Column of table
function TableColumn(name="", type=TableColumnType.NUMERIC, unit='') {
  // Column name
	this.name = name
  // Column values type
	this.type = type
  // Column values unit
	this.unit = unit
}

// Row/entry of table
function TableEntry(table=new Table()) {
  // Values per column
	this.columnValues = table._columns.reduce((accu,curr) => {
		return {...accu,[curr.name]: 0}
	}, {"-":0,})
	delete this.columnValues["-"]
  // Table owning the entry
	this._table = table
}

const TablePackOperation = Object.freeze({
  NONE: "",
  SUM: "sum",
	MEAN: "mean",
  STD: "std",
  MIN: "min",
  MAX: "max",
})

/*Collection of table columns*/
function TableColumnPack(table=new Table()) {
  this.columns = [new TableColumn()]
  this.columns.splice(0,1)
  this._table = table

  this.removeColumn = function (col=new TableColumn()) {
    const index = this.columns.findIndex(c => col==c)
    if(index>=0) this.columns.splice(index,1)
  }
  this.clearColumns = function () {
    this.columns.splice(0,this.columns.length)
  }
  this.addColumn = function (col=new TableColumn()) {
    const index = this.columns.findIndex(c => col==c)
    if(index<0) this.columns.push(col)
  }

  this.operation = TablePackOperation.NONE

  /* Virtual column representing the aggragated values of the columns. */
  this.virtualColumn = new TableColumn()

  var computeSum = (entry=new TableEntry()) =>
    this.columns.reduce((sum, col) => {
      return parseFloat(entry.columnValues[col.name]) + sum
    }, 0)
  var computeMean = (entry=new TableEntry()) =>
    this.columns.reduce((mean, col) => {
      return (1.0/this.columns.length)*parseFloat(entry.columnValues[col.name]) + mean
    }, 0)
  var computeStd = (entry=new TableEntry()) => {
    var mean = computeMean(entry)
    var variance = this.columns.reduce((std, col) => {
      return (1.0/(this.columns.length-1))*Math.pow(parseFloat(entry.columnValues[col.name])-mean,2) + std
    }, 0)
    return Math.sqrt(variance)
  }
  var computeMin = (entry=new TableEntry()) =>
    this.columns.reduce((min, col) => {
      return parseFloat(entry.columnValues[col.name])<min?parseFloat(entry.columnValues[col.name]):min
    }, Number.MAX_VALUE)
  var computeMax = (entry=new TableEntry()) =>
    this.columns.reduce((max, col) => {
      return parseFloat(entry.columnValues[col.name])>max?parseFloat(entry.columnValues[col].name):max
    }, -Number.MAX_VALUE)

  // Return the value of the pack for the specified entry and pack operation
  this.getEntryValue = function (entry=new TableEntry()) {
    switch (this.operation) {
      case TablePackOperation.NONE:
        return 0
      case TablePackOperation.SUM:
        return computeSum(entry)
      case TablePackOperation.MEAN:
        return computeMean(entry)
      case TablePackOperation.STD:
        return computeStd(entry)
      case TablePackOperation.MIN:
        return computeMin(entry)
      case TablePackOperation.MAX:
        return computeMax(entry)
      default:
        return 0
    }
  }
}

function Table () {
	this._columns = [new TableColumn()]
	this._entries = [new TableEntry(this)]
  this._packs = [new TableColumnPack(this)]

	this.clearColumns = function () {
		this._columns.splice(0,this._columns.length)
	}
	this.clearEntries = function () {
		this._entries.splice(0,this._entries.length)
	}
  this.clearPacks = function () {
		this._packs.splice(0,this._packs.length)
	}

  this.addPack = function () {
    const pack = new TableColumnPack(this)
    pack.operation = TablePackOperation.NONE
    pack.virtualColumn.name = ""
    pack.virtualColumn.type = TableColumnType.NUMERIC
    pack.virtualColumn.unit = ''
    this._packs.push(pack)
    return pack
  }
	this.addCol = function (name="", type=TableColumnType.NUMERIC, unit='') {
		const col = new TableColumn(name, type, unit)
		this._columns.push(col)

		this._entries.forEach((entry) => {
			entry.columnValues[name] = 0
		})

		return col
	}
	this.addRow = function () {
		const entry = new TableEntry(this)
		this._entries.push(entry)
		return entry
	}
	this.moveColumn = function (col=new TableColumn(), incr=1) {
		const count = this._columns.length
		const index = this._columns.findIndex((c)=>col==c)
		if(index>=0) {
			const aux = this._columns[index]
			this._columns[index] = this._columns[(index+incr+count)%count]
			this._columns[(index+incr+count)%count] = aux
		}
	}
	this.removeColumn = function (col=new TableColumn()) {
		this._entries.forEach((entry) => {
			delete entry.columnValues[col.name]
		})
		const index = this._columns.findIndex((c)=>col==c)
		if(index>=0) {
			this._columns.splice(index,1)
		}
	}
	this.updateColumn = function (col=new TableColumn(), nameNew="", unitNew="") {
		this._entries.forEach((entry) => {
			const val = entry.columnValues[col.name]
			delete entry.columnValues[col.name]
			entry.columnValues[nameNew] = val
		})
		const index = this._columns.findIndex((c)=>col==c)
		if(index>=0) {
			this._columns[index].name = nameNew
			this._columns[index].unit = unitNew
		}
	}
	this.removeEntry = function (entry=new TableEntry()) {
		const index = this._entries.findIndex((e)=>entry==e)
		if(index>=0) {
			this._entries.splice(index,1)
		}
	}

  // Generate a table containing the pack values
  this.generateTable = function () {
    // New table
    const table = new Table()
    table.clearPacks()
    table.clearColumns()
    table.clearEntries()
    // add columns for the packs
    this._packs.forEach(pack => {
      const vcolname = `${pack.virtualColumn.name} [${pack.operation}]`
      table.addCol(vcolname, pack.virtualColumn.type, pack.virtualColumn.unit)
    })
    // add the entry values 
    this._entries.forEach(entry => {
      const e = table.addRow()
      this._packs.forEach(pack => {
        const vcolname = `${pack.virtualColumn.name} [${pack.operation}]`
        e.columnValues[vcolname] = pack.getEntryValue(entry)
      })
    })
    return table
  }

	this.toJson = function () {
		const json = {columns: [], packs: [], entries: []}
		this._columns.forEach((col) => {
			json.columns.push({
        name: col.name, type: col.type, unit: col.unit
      })
		})
    this._packs.forEach((pack) => {
      json.packs.push({
        operation: pack.operation,
        columnsNames: pack.columns.map(c => c.name),
        virtualColumn: {
          name: pack.virtualColumn.name,
          type: pack.virtualColumn.type,
          unit: pack.virtualColumn.unit
        }
      })
    })
		this._entries.forEach((entry) => {
			json.entries.push(entry.columnValues)
		})
		return json
	}
	this.fromJson = function (json) {
		json.columns.forEach((c) => {
			this.addCol(c.name, c.type, c.unit)
		})
    if(json.packs) {
      json.packs.forEach((p) => {
        let pack = this.addPack()
        pack.operation = p.operation
        pack.virtualColumn.name = p.virtualColumn.name
        pack.virtualColumn.type = p.virtualColumn.type
        pack.virtualColumn.unit = p.virtualColumn.unit
        p.columnsNames.forEach((cName) => {
          col = this._columns.find(cc => cc.name===cName)
          pack.addColumn(col)
        })
      })
    }
		json.entries.forEach((e) => {
			let entry = tableView.table.addRow()
			Object.keys(e).forEach((c) => {
				entry.columnValues[c] = e[c]
			})
		})
	}
}


