// ----------------------------------------------------
// View Viz
// ----------------------------------------------------

const COLORS = [
  '#7FFF00',
  '#D2691E',
  '#008B8B',
  '#006400',
  '#556B2F',
  '#2F4F4F',
  '#008000',
  '#CD5C5C',
  '#4B0082',
  '#800000',
  '#808000'
]

function TableViz(
  canvasId="", canvasContainerId="", table=new Table(),
  labelCol=new TableColumn(),
  valueCols=[new TableColumn()]
) {
  var canvasElem = document.getElementById(canvasId);
  var container = document.getElementById(canvasContainerId);

  var barThickness = 10
  var containerWidth = (2*valueCols.length*barThickness)*table._entries.length
  containerWidth = containerWidth>container.clientWidth?containerWidth:container.clientWidth

  container.style.position = 'relative'
  container.style.width = `${containerWidth}px`

  var chart = new Chart(canvasElem, {
    type: 'bar',
    data: {
      labels: table._entries.map(entry => entry.columnValues[labelCol.name]),
      datasets: valueCols.map((col,index) => {
        return {
          label: col.name,
          data: table._entries.map(entry => entry.columnValues[col.name]),
          borderWidth: 0,
          barThickness: barThickness,
          backgroundColor: COLORS[index%COLORS.length]
        }
      })
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        y: {
          beginAtZero: true,
          ticks: {
            color: 'black',
            font: {size: 15, weight: 'bold'}
          }
        },
        x: {
          grid: {
            display: false
          },
          ticks: {
            color: 'black',
            font: {size: 16, weight: 'bold'}
          },
          title: {
            display: true,
            text: labelCol.name,
            font: {size: 13, weight: 'bold'}
          },
        }
      },
      plugins: {
        tooltip: {
          callbacks: {
            label: function (context) {
              const label = context.dataset.label;
              const value = context.raw
              const valueColIndex = context.datasetIndex
              // const entryIndex = context.dataIndex
              return `${label} : ${value} ${valueCols[valueColIndex].unit}`
            }
          },
          bodyColor: 'white',
          bodyFont: {size: 13, weight: 'bold'}
        }
      },

      events: ['mousemove'],
    },

    plugins: [{
      id: 'colHighlighter',
      highlightedColIndex: -1,
      originalDatasetColors: [],
      beforeInit(chart, args, options) {
        this.originalDatasetColors = chart.data.datasets.map(dataset => dataset.backgroundColor)
      },
      beforeEvent(chart, args, pluginOptions) {
        const event = args.event;
        if(event.type === 'mousemove') {
          const elements = chart.getElementsAtEventForMode(event, 'nearest', { intersect: true }, true)
          var isOnElement = elements.length>0
          if(isOnElement) {
            const element = elements[0]
            const colIndex = element.datasetIndex
            if(this.highlightedColIndex!=colIndex) {
              this.highlightedColIndex = colIndex

              chart.data.datasets.forEach((dataset, i) => {
                if(i==this.highlightedColIndex) {
                  dataset.backgroundColor = 
                    this.originalDatasetColors[i]
                }
                else {
                  dataset.backgroundColor = 
                    this.originalDatasetColors[i]+"50"
                }
              })
              
              chart.update()
            }
          }
          else {
            if(this.highlightedColIndex>=0) {
              chart.data.datasets.forEach((dataset, i) => {
                dataset.backgroundColor = this.originalDatasetColors[i]
              })
              this.highlightedColIndex = -1
              chart.update()
            }
          }
        }
      }
    }]
  });

  this.destroy = function () {
    chart.destroy()
  }
}
